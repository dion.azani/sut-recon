package com.mas.sut.recon.yearrebasing.repository;

import java.util.List;

import com.mas.sut.recon.yearrebasing.model.DsutColRowModel;
import com.mas.sut.recon.yearrebasing.model.ViewBasicPriceModel;

public interface YearRebasingRepository {

	List<DsutColRowModel> getDsutColRowList(String codeCol, String rowCode, long msutId, long roundSutId);
	DsutColRowModel getDsutColRow(String codeCol, String rowCode, long msutId, long roundSutId);
	int insertDsut(long colId, long rowId, long msutId, long msutIdNew);
	String doYearRebasing(long msutId, long basicPriceId, String newName);
	
	List<ViewBasicPriceModel> getViewBasicPriceList(long basicPriceId);
}
