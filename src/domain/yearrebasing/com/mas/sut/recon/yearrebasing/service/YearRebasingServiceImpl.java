package com.mas.sut.recon.yearrebasing.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mas.sut.recon.dto.Response;
import com.mas.sut.recon.dto.ResponseData;
import com.mas.sut.recon.model.BasicPriceModel;
import com.mas.sut.recon.model.DbasicPriceModel;
import com.mas.sut.recon.model.DsutModel;
import com.mas.sut.recon.model.MsutModel;
import com.mas.sut.recon.model.RoundSutModel;
import com.mas.sut.recon.repository.BasicPriceRepository;
import com.mas.sut.recon.repository.DbasicPriceRepository;
import com.mas.sut.recon.repository.DsutColRowRepository;
import com.mas.sut.recon.repository.DsutRepository;
import com.mas.sut.recon.repository.MsutRepository;
import com.mas.sut.recon.repository.RoundSutRepository;
import com.mas.sut.recon.utils.FormatUtils;
import com.mas.sut.recon.yearrebasing.dto.BasicPriceDto;
import com.mas.sut.recon.yearrebasing.dto.DbasicPriceDto;
import com.mas.sut.recon.yearrebasing.dto.ViewBasicPriceDto;
import com.mas.sut.recon.yearrebasing.dto.YearRebasingRequestDto;
import com.mas.sut.recon.yearrebasing.dto.YearRebasingResponseDto;
import com.mas.sut.recon.yearrebasing.model.DsutColRowModel;
import com.mas.sut.recon.yearrebasing.model.ViewBasicPriceModel;
import com.mas.sut.recon.yearrebasing.repository.YearRebasingRepository;

@Service
public class YearRebasingServiceImpl implements YearRebasingService {

	@Autowired
	YearRebasingRepository yearRebasingRepository;
	
	@Autowired
	BasicPriceRepository basicPriceRepository;
	
	@Autowired
	DbasicPriceRepository dbasicPriceRepository;
	
	@Autowired
	RoundSutRepository roundSutRepository;
	
	@Autowired
	MsutRepository msutRepository;
	
	@Autowired
	DsutColRowRepository dsutColRowRepository;
	
	@Autowired
	DsutRepository dsutRepository;
	
	@Override
	public Response ping() {

		FormatUtils fu = new FormatUtils();
		
		// Set default Response.
		Response response = new Response();
		response.setSuccess(true);
		response.setResponseCode("000");
		response.setResponseMessage("");
		response.setClientMessage("ping success");
		response.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return response;
	}

	@Override
	public ResponseData doYearRebasing(YearRebasingRequestDto yearRebasingRequestDto) {
		
		FormatUtils fu = new FormatUtils();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		
		// Set default Response.
		ResponseData response = new ResponseData();
		response.setSuccess(false);
		response.setResponseCode("002");
		response.setResponseMessage("");
		response.setData(null);
		
		long msutId = yearRebasingRequestDto.getMsutId();
		long basicPriceId = yearRebasingRequestDto.getBasicPriceId();
		String newName = yearRebasingRequestDto.getNewName();
		
		RoundSutModel roundSutModel = this.roundSutRepository.getRoundSutByMsutIdOrderByRoundDesc(msutId);
		long roundSutId = roundSutModel.getId();
		
		year = year + 1;
		
		// Create new sut
		MsutModel msutModel = new MsutModel();
		msutModel.setName(newName);
		msutModel.setDescription(newName);
		msutModel.setYear(String.valueOf(year));
		msutModel.setCreatedAt(fu.getCurrentTimestamp());
		long msutIdNew = this.msutRepository.insert(msutModel);
		System.out.println("Create new SUT : " + msutIdNew);
		
		// Create new roundSut
		short round = 0;
		roundSutModel = new RoundSutModel();
		roundSutModel.setMsutId(msutIdNew);
		roundSutModel.setRound(round);
		roundSutModel.setCreatedAt(fu.getCurrentTimestamp());
		long roundSutIdNew = this.roundSutRepository.insert(roundSutModel);
		System.out.println("Create new roundSut " + roundSutIdNew);
		
		List<DbasicPriceModel> dbasicPriceModelList = this.dbasicPriceRepository.getDbasicPriceByBasicPriceId(basicPriceId);
		System.out.println("get dbasicPriceModelList " + dbasicPriceModelList.size());

		for(DbasicPriceModel dbasicPriceModel : dbasicPriceModelList) {
			
			System.out.println("Iterate DbasicPriceModel : " + dbasicPriceModel.getCodeCol() + " and " + dbasicPriceModel.getCodeRow());
			
			String codeCol = dbasicPriceModel.getCodeCol();
			String codeRow = dbasicPriceModel.getCodeRow();
			double deflatorValue = dbasicPriceModel.getDeflatorValue();
			
			System.out.println("Query DsutColRow, dengan msut_id : " + msutId + " dan roundSutId : " + roundSutId);
			
			List<DsutColRowModel> dsutColRowModelList = this.yearRebasingRepository.getDsutColRowList(codeCol, 
																							codeRow,
																							msutId,
																							roundSutId);
			
			System.out.println("get dsutColRowList, start to insert to dsut. Jumlah data :  " + dsutColRowModelList.size());
			
			for(DsutColRowModel dsutColRowModel : dsutColRowModelList) {
				
				System.out.println("Iterate DsutColRowModelList : " + dsutColRowModel.getId() + " value : " + dsutColRowModel.getValue());
				
				// Insert ke Dsut dulu.
				List<DsutModel> dsutModelList = this.dsutRepository.getDsut(dsutColRowModel.getDsutColId(), dsutColRowModel.getDsutRowId(), msutIdNew);
				System.out.println("get dsutModelList " + dsutModelList.size());
				
				// Insert ke Dsut dulu, col.
				DsutModel dsutModelToInsert = new DsutModel();
				dsutModelToInsert.setName(dsutModelList.get(0).getName());
				dsutModelToInsert.setDisplayName(dsutModelList.get(0).getDisplayName());
				dsutModelToInsert.setCode(dsutModelList.get(0).getCode());
				dsutModelToInsert.setParentCode(dsutModelList.get(0).getParentCode());
				dsutModelToInsert.setMsutId(msutIdNew);
				dsutModelToInsert.setOtorizationUserId(dsutModelList.get(0).getOtorizationUserId());
				dsutModelToInsert.setLevel(dsutModelList.get(0).getLevel());
				dsutModelToInsert.setTotal(dsutModelList.get(0).isTotal());
				dsutModelToInsert.setNameShort(dsutModelList.get(0).getNameShort());
				dsutModelToInsert.setCreatedAt(fu.getCurrentTimestamp());
				dsutModelToInsert.setCreatedBy(dsutModelList.get(0).getCreatedBy());
				dsutModelToInsert.setUpdatedAt(fu.getCurrentTimestamp());
				dsutModelToInsert.setUpdatedBy(dsutModelList.get(0).getUpdatedBy());
				dsutModelToInsert.setDbmetadataId(dsutModelList.get(0).getDbmetadataId());
				dsutModelToInsert.setParentCode(dsutModelList.get(0).getParentCode());
				dsutModelToInsert.setRowMetadataId(dsutModelList.get(0).getRowMetadataId());
				dsutModelToInsert.setColMetadataId(dsutModelList.get(0).getColMetadataId());
				dsutModelToInsert.setSutCode(dsutModelList.get(0).getSutCode());
				dsutModelToInsert.setTotalFormula(dsutModelList.get(0).getTotalFormula());
				dsutModelToInsert.setDmetadataId(dsutModelList.get(0).getDmetadataId());
				dsutModelToInsert.setClassificationId(dsutModelList.get(0).getClassificationId());
				long colIdNew = this.dsutRepository.insert(dsutModelToInsert);
				System.out.println("create newDsut 01 : " + colIdNew);
				
				// Insert ke Dsut dulu, row.
				dsutModelToInsert = new DsutModel();
				dsutModelToInsert.setName(dsutModelList.get(1).getName());
				dsutModelToInsert.setDisplayName(dsutModelList.get(1).getDisplayName());
				dsutModelToInsert.setCode(dsutModelList.get(1).getCode());
				dsutModelToInsert.setParentCode(dsutModelList.get(1).getParentCode());
				dsutModelToInsert.setMsutId(msutIdNew);
				dsutModelToInsert.setOtorizationUserId(dsutModelList.get(1).getOtorizationUserId());
				dsutModelToInsert.setLevel(dsutModelList.get(1).getLevel());
				dsutModelToInsert.setTotal(dsutModelList.get(1).isTotal());
				dsutModelToInsert.setNameShort(dsutModelList.get(1).getNameShort());
				dsutModelToInsert.setCreatedAt(fu.getCurrentTimestamp());
				dsutModelToInsert.setCreatedBy(dsutModelList.get(1).getCreatedBy());
				dsutModelToInsert.setUpdatedAt(fu.getCurrentTimestamp());
				dsutModelToInsert.setUpdatedBy(dsutModelList.get(1).getUpdatedBy());
				dsutModelToInsert.setDbmetadataId(dsutModelList.get(1).getDbmetadataId());
				dsutModelToInsert.setParentCode(dsutModelList.get(1).getParentCode());
				dsutModelToInsert.setRowMetadataId(dsutModelList.get(1).getRowMetadataId());
				dsutModelToInsert.setColMetadataId(dsutModelList.get(1).getColMetadataId());
				dsutModelToInsert.setSutCode(dsutModelList.get(1).getSutCode());
				dsutModelToInsert.setTotalFormula(dsutModelList.get(1).getTotalFormula());
				dsutModelToInsert.setDmetadataId(dsutModelList.get(1).getDmetadataId());
				dsutModelToInsert.setClassificationId(dsutModelList.get(1).getClassificationId());
				long rowIdNew = this.dsutRepository.insert(dsutModelToInsert);
				System.out.println("create newDsut 02 : " + rowIdNew);
				
				// Calculate DsutColRow value, with deflator_value.
				double valueNew = (100 / deflatorValue) * dsutColRowModel.getValue();
				
				com.mas.sut.recon.model.DsutColRowModel dsutColRowModelToInsert = 
						new com.mas.sut.recon.model.DsutColRowModel();
				
				// Insert into DsutColRowModel
				dsutColRowModelToInsert.setDsutColId(colIdNew);
				dsutColRowModelToInsert.setDsutRowId(rowIdNew);
				dsutColRowModelToInsert.setMSutId(msutIdNew);
				dsutColRowModelToInsert.setRoundSutId(roundSutIdNew);
				dsutColRowModelToInsert.setValue(valueNew);
				dsutColRowModelToInsert.setCreatedAt(fu.getCurrentTimestamp());
				
				this.dsutColRowRepository.insert(dsutColRowModelToInsert);
				System.out.println("create new dsutColRow for colId : " + colIdNew + " and rowId : " + rowIdNew);
			}
		}
		
		// prepare to set data.
		List<YearRebasingResponseDto> yearRebasingResponseDtoList = new ArrayList<YearRebasingResponseDto>();
		
		YearRebasingResponseDto yearRebasingResponseDto = new YearRebasingResponseDto();
		yearRebasingResponseDto.setMsutIdNew(msutIdNew);
		yearRebasingResponseDto.setRoundSutIdNew(roundSutIdNew);
		
		yearRebasingResponseDtoList.add(yearRebasingResponseDto);
		
		// set Response.
		response.setSuccess(true);
		response.setResponseCode("000");
		response.setResponseMessage("");
		response.setData(yearRebasingResponseDtoList);
		
		System.out.println("Finish");
		
		// set timestamp of Response.
		response.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return response;
	}

	@Override
	@Transactional
	public Response createBasicPrice(BasicPriceDto basicPriceDto) {
		
		FormatUtils fu = new FormatUtils();
		
		// Set default Response.
		Response response = new Response();
		response.setSuccess(false);
		response.setResponseCode("002");
		response.setResponseMessage("");
				
		BasicPriceModel basicPriceModel = new BasicPriceModel();
		basicPriceModel.setName(basicPriceDto.getName());
		basicPriceModel.setYear(basicPriceDto.getYear());
		
		long basicPriceId = this.basicPriceRepository.insert(basicPriceModel);
				
		List<DbasicPriceDto> dBasicPriceDtoList = basicPriceDto.getDbasicPriceDtoList();
		for(DbasicPriceDto dto : dBasicPriceDtoList) {
			DbasicPriceModel model = new DbasicPriceModel();
			model.setCodeCol(dto.getCodeCol());
			model.setCodeRow(dto.getCodeRow());
			model.setDeflatorValue(Integer.valueOf(dto.getDeflatorValue()));
			model.setBasicPriceId(basicPriceId);
			long dBasicPriceId = this.dbasicPriceRepository.insert(model);
		}
		
		response.setSuccess(true);
		response.setResponseCode("000");
		response.setResponseMessage("");
		
		// set timestamp of Response.
		response.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return response;
	}

	@Override
	@Async
	public CompletableFuture<String> asyncDoYearRebasing(long msutId, long basicPriceId, String newName) {
		
		String messages = this.yearRebasingRepository.doYearRebasing(msutId, basicPriceId, newName);
		
		// set timestamp of Response.
		FormatUtils fu = new FormatUtils();

		return CompletableFuture.completedFuture(messages);
	}

	@Override
	public ResponseData doYearRebasing_v3(long msutId, long basicPriceId, String newName) {
		
		ResponseData responseData = new ResponseData();
		responseData.setResponseCode("000");
		responseData.setClientMessage("");
		
		String messages = this.yearRebasingRepository.doYearRebasing(msutId, basicPriceId, newName);
		responseData.setSuccess(true);
		
		// set timestamp of Response.
		FormatUtils fu = new FormatUtils();
		responseData.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return responseData;
	}

	@Override
	public Response getBasicPriceById(long basicPriceId) {
		// TODO Auto-generated method stub
		BasicPriceModel basicPriceModel = this.basicPriceRepository.getBasicPriceById(basicPriceId);
		
		List<BasicPriceDto> basicPriceDtoList = new ArrayList<BasicPriceDto>();
		
		BasicPriceDto basicPriceDto = new BasicPriceDto();
		basicPriceDto.setId(basicPriceModel.getId());
		basicPriceDto.setName(basicPriceModel.getName());
		basicPriceDto.setYear(basicPriceModel.getYear());
		basicPriceDto.setMetadataId(basicPriceModel.getMetadataId());
		
		basicPriceDtoList.add(basicPriceDto);
		
		ResponseData responseData = new ResponseData();
		responseData.setResponseCode("000");
		responseData.setClientMessage("");
		responseData.setData(basicPriceDtoList);
		
		// set timestamp of Response.
		FormatUtils fu = new FormatUtils();
		responseData.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return responseData;
	}

	@Override
	public Response updateBasicPrice(BasicPriceDto basicPriceDto) {
		BasicPriceModel basicPriceModel = new BasicPriceModel();
		basicPriceModel.setId(basicPriceDto.getId());
		basicPriceModel.setName(basicPriceDto.getName());
		basicPriceModel.setYear(basicPriceDto.getYear());
		basicPriceModel.setMetadataId(basicPriceDto.getMetadataId());
		
		long id = this.basicPriceRepository.update(basicPriceModel);
		
		Response response = new Response();
		response.setSuccess(true);
		response.setResponseCode("000");
		response.setResponseMessage("");
		
		return response;
	}

	@Override
	public Response deleteBasicPrice(long basicPriceId) {
		long id = this.basicPriceRepository.delete(basicPriceId);
		
		Response response = new Response();
		response.setSuccess(true);
		response.setResponseCode("000");
		response.setResponseMessage("");
		
		return response;
	}

	@Override
	public ResponseData getBasicPrice() {
		
		List<BasicPriceModel> basicPriceModelList = this.basicPriceRepository.getBasicPrice();
		List<BasicPriceDto> basicPriceDtoList = new ArrayList<BasicPriceDto>();
		
		for (BasicPriceModel model : basicPriceModelList) {
			BasicPriceDto basicPriceDto = new BasicPriceDto();
			basicPriceDto.setId(model.getId());
			basicPriceDto.setName(model.getName());
			basicPriceDto.setYear(model.getYear());
			basicPriceDto.setMetadataId(model.getMetadataId());
			
			basicPriceDtoList.add(basicPriceDto);
		}
		
		ResponseData responseData = new ResponseData();
		responseData.setResponseCode("000");
		responseData.setClientMessage("");
		responseData.setData(basicPriceDtoList);
		
		// set timestamp of Response.
		FormatUtils fu = new FormatUtils();
		responseData.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return responseData; 
	}

	@Override
	public ResponseData getViewBasicPriceList(long basicPriceId) {
		FormatUtils fu = new FormatUtils();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		
		// Set default Response.
		ResponseData response = new ResponseData();
		response.setSuccess(false);
		response.setResponseCode("002");
		response.setResponseMessage("");
		response.setData(null);
		
		List<ViewBasicPriceModel> viewBasicPriceModelList = this.yearRebasingRepository.getViewBasicPriceList(basicPriceId);
		List<ViewBasicPriceDto> viewBasicPriceDtoList = new ArrayList<ViewBasicPriceDto>();
		
		for(ViewBasicPriceModel model : viewBasicPriceModelList) {
			ViewBasicPriceDto dto = new ViewBasicPriceDto();
			dto.setCodeCol(model.getCodeCol());
			dto.setNameCol(model.getNameCol());
			dto.setCodeRow(model.getCodeRow());
			dto.setNameRow(model.getNameRow());
			dto.setDeflatorValue(model.getDeflatorValue());
			dto.setMetaDataId(model.getMetaDataId());
			
			viewBasicPriceDtoList.add(dto);
		}
		
		response = new ResponseData();
		response.setSuccess(true);
		response.setResponseCode("000");
		response.setResponseMessage("");
		response.setData(viewBasicPriceDtoList);
		
		return response;
	}
}
