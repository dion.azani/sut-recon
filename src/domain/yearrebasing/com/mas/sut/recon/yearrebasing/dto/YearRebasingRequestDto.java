package com.mas.sut.recon.yearrebasing.dto;

import lombok.Data;

@Data
public class YearRebasingRequestDto {
	private long msutId;
	private long basicPriceId;
	private String newName;
}
