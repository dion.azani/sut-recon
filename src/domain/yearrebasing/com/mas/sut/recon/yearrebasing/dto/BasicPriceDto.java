package com.mas.sut.recon.yearrebasing.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BasicPriceDto {

	private long id;
	private String name;
	private int year;
	private long metadataId;
	@JsonProperty("dbasicPrice")
	private List<DbasicPriceDto> dbasicPriceDtoList;
	
}
