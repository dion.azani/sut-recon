package com.mas.sut.recon.yearrebasing.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViewBasicPriceDto {
	
	private String codeCol;
	private String nameCol;
	private String codeRow;
	private String nameRow;
	public int deflatorValue;
	long metaDataId;
}
