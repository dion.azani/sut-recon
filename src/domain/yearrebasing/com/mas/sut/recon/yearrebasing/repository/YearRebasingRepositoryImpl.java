package com.mas.sut.recon.yearrebasing.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mas.sut.recon.model.DbasicPriceModel;
import com.mas.sut.recon.yearrebasing.model.DsutColRowModel;
import com.mas.sut.recon.yearrebasing.model.ViewBasicPriceModel;

@Repository
public class YearRebasingRepositoryImpl implements YearRebasingRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public List<DsutColRowModel> getDsutColRowList(String codeCol, String rowCode, long msutId, long roundSutId) {
		
		List<DsutColRowModel> dsutColRowModelList = new ArrayList<DsutColRowModel>();
		
		String sql = "select dsutcolrow.id, " + 
				"		dsutcolrow.value, " + 
				"		dsutcolrow.dsutcol_id," + 
				"		dsutcolrow.dsutrow_id " + 
				"from dsutcolrow inner join dsut col on col.id = dsutcolrow.dsutcol_id " + 
				"		inner join dsut bar on bar.id = dsutcolrow.dsutrow_id " + 
				"where substring(col.code, 1, 6) = :colId " + 
				"		and substring(bar.code, 1, 6) = :rowId " + 
				"		and dsutcolrow.msut_id = :msutId " + 
				"		and dsutcolrow.roundsut_id = :roundSutId " + 
				"order by dsutcolrow.value desc ";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("colId", codeCol);
        mapSqlParameterSource.addValue("rowId", rowCode);
        mapSqlParameterSource.addValue("msutId", msutId);
        mapSqlParameterSource.addValue("roundSutId", roundSutId);
        
        dsutColRowModelList = namedParameterJdbcTemplate.query(sql, 
						mapSqlParameterSource, 
						(rs, rowNum) ->
									        new DsutColRowModel (rs.getLong("id"),
													rs.getDouble("value"),
													rs.getLong("dsutcol_id"),
													rs.getLong("dsutrow_id"))
                        );
		return dsutColRowModelList;
	}
	
	@Override
	public DsutColRowModel getDsutColRow(String codeCol, String rowCode, long msutId, long roundSutId) {
		
		DsutColRowModel dsutColRowModel = null;
		
		String sql = "select dsutcolrow.id," + 
				"		dsutcolrow.value," + 
				"		dsutcolrow.dsutcol_id," + 
				"		dsutcolrow.dsutrow_id" + 
				"from dsutcolrow inner join dsut col on col.id = dsutcolrow.dsutcol_id" + 
				"		inner join dsut bar on bar.id = dsutcolrow.dsutrow_id" + 
				"where substring(col.code, 1, 6) = :colId" + 
				"		and substring(bar.code, 1, 6) = :rowId" + 
				"		and dsutcolrow.msut_id = :msutId" + 
				"		and dsutcolrow.roundsut_id = :roundSutId" + 
				"order by dsutcolrow.value desc ";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("colId", codeCol);
        mapSqlParameterSource.addValue("rowId", rowCode);
        mapSqlParameterSource.addValue("msutId", msutId);
        mapSqlParameterSource.addValue("roundSutId", roundSutId);
        
        try {
        	dsutColRowModel = namedParameterJdbcTemplate.queryForObject(sql, 
					mapSqlParameterSource, 
					(rs, rowNum) ->
							new DsutColRowModel (rs.getLong("id"),
													rs.getDouble("value"),
													rs.getLong("dsutcol_id"),
													rs.getLong("dsutrow_id"))
                    );
        }
        catch(DataAccessException dae) {
        	System.out.println("Error in class YearRebasingRepositoryImpl.DsutColRowModel");
        }
        
		return dsutColRowModel;
	}

	@Override
	public int insertDsut(long colId, long rowId, long msutId, long msutIdNew) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String doYearRebasing(long msutId, long basicPriceId, String newName) {
		
		String messages = "true";
		
		// msutid_p bigint, basicpriceid_p bigint, newname_p character varying
		final SimpleJdbcCall doYearRebasingCall = new SimpleJdbcCall(jdbcTemplate).withFunctionName("do_year_rebasing");
		final Map<String, Object> params = new HashMap<>();
		params.put("msutid_p", msutId);
		params.put("basicpriceid_p", basicPriceId);
		params.put("newname_p", newName);

		final Map<String, Object> result = doYearRebasingCall.execute(params);
		
		return messages;
	}

	@Override
	public List<ViewBasicPriceModel> getViewBasicPriceList(long basicPriceId) {
		
		List<ViewBasicPriceModel> viewBasicPriceModelList = new ArrayList<ViewBasicPriceModel>();
		
		String sql = "select dbasicprice.code_col,\n" + 
				"		dbmetadata.\"name\" as name_col, \n" + 
				"		dbasicprice.code_row,\n" + 
				"		dbmetadata2.\"name\" as name_row, \n" + 
				"		dbasicprice.deflatorvalue, \n" + 
				"		basicprice.metadata_id \n" + 
				"from dbasicprice inner join basicprice on basicprice.id = dbasicprice.basicprice_id\n" + 
				"		inner join dbmetadata on dbmetadata.code = dbasicprice.code_col and dbmetadata.metadata_id = basicprice.metadata_id\n" + 
				"		inner join dbmetadata dbmetadata2 on dbmetadata2.code = dbasicprice.code_row and dbmetadata2.metadata_id = basicprice.metadata_id\n" + 
				"where dbasicprice.basicprice_id = :basicPriceId \n" + 
				"order by dbasicprice.code_col, dbasicprice.code_row";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("basicPriceId", basicPriceId);
        
        viewBasicPriceModelList = namedParameterJdbcTemplate.query(sql, 
				mapSqlParameterSource, 
				(rs, rowNum) ->
							        new ViewBasicPriceModel (rs.getString("code_col"),
							        							rs.getString("name_col"),
							        							rs.getString("code_row"),
							        							rs.getString("name_col"),
							        							rs.getInt("deflatorvalue"),
							        							rs.getLong("metadata_id"))
                );
        
		return viewBasicPriceModelList;
	}

}
