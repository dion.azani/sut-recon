package com.mas.sut.recon.yearrebasing.dto;

import lombok.Data;

@Data
public class DbasicPriceDto {

	private String codeCol;
	private String codeRow;
	private String deflatorValue;
	private long basicPriceId;
}
