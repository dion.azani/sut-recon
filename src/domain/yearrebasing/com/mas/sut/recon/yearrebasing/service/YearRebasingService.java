package com.mas.sut.recon.yearrebasing.service;

import java.util.concurrent.CompletableFuture;

import com.mas.sut.recon.dto.Response;
import com.mas.sut.recon.dto.ResponseData;
import com.mas.sut.recon.yearrebasing.dto.BasicPriceDto;
import com.mas.sut.recon.yearrebasing.dto.YearRebasingRequestDto;

public interface YearRebasingService {

	Response ping();
	ResponseData doYearRebasing(YearRebasingRequestDto yearRebasingRequestDto);
	CompletableFuture<String> asyncDoYearRebasing(long msutId, long basicPriceId, String newName);
	ResponseData doYearRebasing_v3(long msutId, long basicPriceId, String newName);
	Response createBasicPrice(BasicPriceDto basicPriceDto);
	
	Response getBasicPriceById(long basicPriceId);
	Response updateBasicPrice(BasicPriceDto basicPriceDto);
	Response deleteBasicPrice(long basicPriceId);
	
	ResponseData getBasicPrice();
	ResponseData getViewBasicPriceList(long basicPriceId);
}
