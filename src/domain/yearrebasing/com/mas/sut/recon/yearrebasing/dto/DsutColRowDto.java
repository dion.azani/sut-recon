package com.mas.sut.recon.yearrebasing.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DsutColRowDto {

	private long id;
	private double value;
	private double newValue;
}
