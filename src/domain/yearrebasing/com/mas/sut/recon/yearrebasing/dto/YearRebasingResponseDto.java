package com.mas.sut.recon.yearrebasing.dto;

import lombok.Data;

@Data
public class YearRebasingResponseDto {

	private long msutIdNew;
	private long roundSutIdNew;
}
