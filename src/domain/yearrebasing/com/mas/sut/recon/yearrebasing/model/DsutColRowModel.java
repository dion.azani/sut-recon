package com.mas.sut.recon.yearrebasing.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DsutColRowModel {

	private long id;
	private double value;
	private long dsutColId;
	private long dsutRowId;

}
