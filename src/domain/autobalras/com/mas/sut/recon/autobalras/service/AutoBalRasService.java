package com.mas.sut.recon.autobalras.service;

import com.mas.sut.recon.autobalras.dto.AutoBalRasDto;
import com.mas.sut.recon.dto.Response;

public interface AutoBalRasService {

	Response ping();
	Response doAutoBalRas(AutoBalRasDto autoBalRasDto);
	Response doAutoBalRasNm(AutoBalRasDto autoBalRasDto);
}
