package com.mas.sut.recon.autobalras.dto;

import lombok.Data;

@Data
public class AutoBalRasDto {

	private long msutId;
	private long roundSutId;
}
