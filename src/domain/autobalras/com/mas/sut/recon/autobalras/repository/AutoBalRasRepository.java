package com.mas.sut.recon.autobalras.repository;

import com.mas.sut.recon.autobalras.dto.AutoBalRasDto;

public interface AutoBalRasRepository {

	String doAutoBalRas(long msutId, long roundSutId);
	String doAutoBalRasNm(long msutId, long roundSutId);
}
