package com.mas.sut.recon.autobalras.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.mas.sut.recon.autobalras.dto.AutoBalRasDto;

@Repository
public class AutoBalRasRepositoryImpl implements AutoBalRasRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public String doAutoBalRas(long msutId, long roundSutId) {
		String messages = "true";
		
		// msutid_p bigint, basicpriceid_p bigint, newname_p character varying
		final SimpleJdbcCall doAutoBalRasCall = new SimpleJdbcCall(jdbcTemplate).withFunctionName("do_autbal_ras");
		final Map<String, Object> params = new HashMap<>();
		params.put("msutIdPar", msutId);
		params.put("roundSutIdPar", roundSutId);

		final Map<String, Object> result = doAutoBalRasCall.execute(params);
		
		return messages;
	}

	@Override
	public String doAutoBalRasNm(long msutId, long roundSutId) {
		String messages = "true";
		
		// msutid_p bigint, basicpriceid_p bigint, newname_p character varying
		final SimpleJdbcCall doAutoBalRasCall = new SimpleJdbcCall(jdbcTemplate).withFunctionName("do_autbal_ras_nm");
		final Map<String, Object> params = new HashMap<>();
		params.put("msutIdPar", msutId);
		params.put("roundSutIdPar", roundSutId);

		final Map<String, Object> result = doAutoBalRasCall.execute(params);
		
		return messages;
	}

}
