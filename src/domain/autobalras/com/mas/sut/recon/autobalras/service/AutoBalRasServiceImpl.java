package com.mas.sut.recon.autobalras.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mas.sut.recon.autobalras.dto.AutoBalRasDto;
import com.mas.sut.recon.autobalras.repository.AutoBalRasRepository;
import com.mas.sut.recon.dto.Response;
import com.mas.sut.recon.dto.ResponseData;
import com.mas.sut.recon.utils.FormatUtils;

@Service
public class AutoBalRasServiceImpl implements AutoBalRasService {

	@Autowired
	AutoBalRasRepository autoBalRasRepository;
	
	@Override
	public Response doAutoBalRas(AutoBalRasDto autoBalRasDto) {
		Response response = new ResponseData();
		response.setResponseCode("000");
		response.setClientMessage("");
		
		String messages = this.autoBalRasRepository.doAutoBalRas(autoBalRasDto.getMsutId(), autoBalRasDto.getRoundSutId());
		response.setSuccess(true);
		
		// set timestamp of Response.
		FormatUtils fu = new FormatUtils();
		response.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return response;
	}

	@Override
	public Response ping() {
		FormatUtils fu = new FormatUtils();
		
		// Set default Response.
		Response response = new Response();
		response.setSuccess(true);
		response.setResponseCode("000");
		response.setResponseMessage("");
		response.setClientMessage("ping success");
		response.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return response;
	}

	@Override
	public Response doAutoBalRasNm(AutoBalRasDto autoBalRasDto) {
		Response response = new ResponseData();
		response.setResponseCode("000");
		response.setClientMessage("");
		
		String messages = this.autoBalRasRepository.doAutoBalRasNm(autoBalRasDto.getMsutId(), autoBalRasDto.getRoundSutId());
		response.setSuccess(true);
		
		// set timestamp of Response.
		FormatUtils fu = new FormatUtils();
		response.setTimeStamp(fu.getCurrentTimestamp().getTime());
		
		return response;
	}

}
