package com.mas.sut.recon.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mas.sut.recon.model.DsutColRowModel;
import com.mas.sut.recon.repository.DsutColRowRepository;

@Repository
public class DsutColRowRepositoryImpl implements DsutColRowRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public int insert(DsutColRowModel dsutColRowModel) {
		int result = 0;
		
		String sql = "INSERT INTO dsutcolrow (dsutcol_id, dsutrow_id, value, msut_id, roundsut_id, created_at) " +
						"VALUES (:dsutcol_id, :dsutrow_id, :value, :msut_id, :roundsut_id, :created_at)";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("dsutcol_id", dsutColRowModel.getDsutColId());
		mapSqlParameterSource.addValue("dsutrow_id", dsutColRowModel.getDsutRowId());
		mapSqlParameterSource.addValue("value", dsutColRowModel.getValue());
		mapSqlParameterSource.addValue("msut_id", dsutColRowModel.getMSutId());
		mapSqlParameterSource.addValue("roundsut_id", dsutColRowModel.getRoundSutId());
		mapSqlParameterSource.addValue("created_at", dsutColRowModel.getCreatedAt());
		
		result = namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
		
		return result;
	}

	@Override
	public int update(DsutColRowModel dsutColRowModel) {
		// TODO Auto-generated method stub
		return 0;
	}

}
