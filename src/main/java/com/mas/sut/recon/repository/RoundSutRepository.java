package com.mas.sut.recon.repository;

import com.mas.sut.recon.model.RoundSutModel;

public interface RoundSutRepository {

	long insert(RoundSutModel roundSutModel);
	RoundSutModel getRoundSutByMsutIdOrderByRoundDesc(long msutId);
}
