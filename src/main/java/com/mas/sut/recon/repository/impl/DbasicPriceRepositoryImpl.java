package com.mas.sut.recon.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mas.sut.recon.model.BasicPriceModel;
import com.mas.sut.recon.model.DbasicPriceModel;
import com.mas.sut.recon.repository.DbasicPriceRepository;

@Repository
public class DbasicPriceRepositoryImpl implements DbasicPriceRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public List<DbasicPriceModel> getDbasicPriceByBasicPriceId(long basicPriceId) {
		List<DbasicPriceModel> dbasicPriceModelList = new ArrayList<DbasicPriceModel>();
		
		String sql = "select dbasicprice.code_col, " + 
				"		dbasicprice.code_row, " + 
				"		dbasicprice.deflatorvalue, " +
				"		dbasicprice.basicprice_id " +
				"from dbasicprice " + 
				"where dbasicprice.basicprice_id = :basicpriceId ";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("basicpriceId", basicPriceId);
        
        dbasicPriceModelList = namedParameterJdbcTemplate.query(sql, 
						mapSqlParameterSource, 
						(rs, rowNum) ->
								new DbasicPriceModel (rs.getString("code_col"),
														rs.getString("code_row"),
														rs.getInt("deflatorvalue"),
														rs.getLong("basicprice_id"))
                        );
		
		return dbasicPriceModelList;
	}

	@Override
	public long insert(DbasicPriceModel dbasicPriceModel) {
		long id = 0;
		
		String sql = "INSERT INTO dbasicprice (code_col, code_row, deflatorvalue, basicprice_id) VALUES (?,?,?,?)";
		
		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {           

		                @Override
		                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		                	
		                    PreparedStatement ps = connection.prepareStatement(sql.toString(),
		                    		new String[] { "id" /* name of your id column */ });
		                    
		                    ps.setString(1, dbasicPriceModel.getCodeCol());
		                    ps.setString(2, dbasicPriceModel.getCodeRow());
		                    ps.setInt(3, dbasicPriceModel.getDeflatorValue());
		                    ps.setLong(4, dbasicPriceModel.getBasicPriceId());
		                    
		                    return ps;
		                }
		            }, holder);

		id = holder.getKey().longValue();
		
		return id;
	}

	@Override
	public long update(DbasicPriceModel dbasicPriceModel) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long delete(long dbasicPriceId) {
		// TODO Auto-generated method stub
		return 0;
	}

}
