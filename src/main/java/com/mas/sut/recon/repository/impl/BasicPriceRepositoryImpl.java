package com.mas.sut.recon.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.mas.sut.recon.model.BasicPriceModel;
import com.mas.sut.recon.repository.BasicPriceRepository;

@Repository
public class BasicPriceRepositoryImpl implements BasicPriceRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public List<BasicPriceModel> getBasicPrice() {
		// TODO Auto-generated method stub
		
		String sql = "select basicprice.id, " + 
				"		basicprice.\"name\", " + 
				"		basicprice.year, " + 
				"		basicprice.metadata_id " + 
				"from basicprice\r\n";

		List<BasicPriceModel> basicPriceModelList = jdbcTemplate.query(sql, 
				(rs, rowNum) ->
						new BasicPriceModel (rs.getLong("id"),
												rs.getString("name"),
												rs.getInt("year"),
												rs.getLong("metadata_id"))
                );

		return basicPriceModelList;
	}
	
	@Override
	public BasicPriceModel getBasicPriceById(long basicPriceId) {
		// TODO Auto-generated method stub
		
		String sql = "select basicprice.id, " + 
				"		basicprice.\"name\", " + 
				"		basicprice.year, " + 
				"		basicprice.metadata_id " + 
				"from basicprice\r\n" + 
				"where basicprice.id = :basicpriceId ";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("basicpriceId", basicPriceId);
        
        BasicPriceModel basicPriceModel = namedParameterJdbcTemplate.queryForObject(sql, 
				mapSqlParameterSource, 
				(rs, rowNum) ->
						new BasicPriceModel (rs.getLong("id"),
												rs.getString("name"),
												rs.getInt("year"),
												rs.getLong("metadata_id"))
                );
        
		return basicPriceModel;
	}
	
	@Override
	public long insert(BasicPriceModel basicPriceModel) {
		
		long id = 0;
		
		String sql = "insert into basicprice (name, year, metadata_id) values (?, ?, ?)";
		
		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {           

		                @Override
		                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		                	
		                    PreparedStatement ps = connection.prepareStatement(sql.toString(),
		                    		new String[] { "id" /* name of your id column */ });
		                    
		                    ps.setString(1, basicPriceModel.getName());
		                    ps.setInt(2, basicPriceModel.getYear());
		                    ps.setLong(3, basicPriceModel.getMetadataId());

		                    return ps;
		                }
		            }, holder);

		id = holder.getKey().longValue();
		
		return id;

	}

	@Override
	public long update(BasicPriceModel basicPriceModel) {
		long id = 0;
		
		String sql = "UPDATE basicprice set name = ?, "
				+ "							year = ?, "
				+ "							metadata_id = ? "
				+ "	WHERE id = ?";
		
		jdbcTemplate.update(sql, basicPriceModel.getName(), 
									basicPriceModel.getYear(), 
									basicPriceModel.getMetadataId(), 
									basicPriceModel.getId());
		id = 1;
		
		return id;
	}

	@Override
	@Transactional
	public long delete(long basicPriceId) {
		long id = 0;
		
		String sql1 = "DELETE FROM dbasicprice where basicprice_id = ?";
		
		jdbcTemplate.update(sql1, basicPriceId);
		
		String sql2 = "DELETE FROM basicprice where id = ?";
		
		jdbcTemplate.update(sql2, basicPriceId);
		
		id = 1;
		
		return id;
	}

}
