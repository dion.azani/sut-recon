package com.mas.sut.recon.repository;

import java.util.List;

import com.mas.sut.recon.model.DsutModel;

public interface DsutRepository {

	List<DsutModel> getDsut(String codeCol, String codeRow, long msutId);
	List<DsutModel> getDsut(long colId, long rowId, long msutId);
	long insert(DsutModel dsutModel);
}
