package com.mas.sut.recon.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mas.sut.recon.model.RoundSutModel;
import com.mas.sut.recon.repository.RoundSutRepository;

@Repository
public class RoundSutRepositoryImpl implements RoundSutRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public long insert(RoundSutModel roundSutModel) {
		long id = 0;
		
		String sql = "INSERT INTO roundsut (msut_id, round, created_at) " +
				"VALUES (?, ?, ?)";
		
		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {           

		                @Override
		                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		                	
		                    PreparedStatement ps = connection.prepareStatement(sql.toString(),
		                    		new String[] { "id" /* name of your id column */ });
		                    
		                    ps.setLong(1, roundSutModel.getMsutId());
		                    ps.setShort(2, roundSutModel.getRound());
		                    ps.setTimestamp(3, roundSutModel.getCreatedAt());

		                    return ps;
		                }
		            }, holder);

		id = holder.getKey().longValue();
		
		return id;
	}

	@Override
	public RoundSutModel getRoundSutByMsutIdOrderByRoundDesc(long msutId) {
		
		RoundSutModel roundSutModel = null;
		
		String sql = "select roundsut.id, roundsut.msut_id, roundsut.round, roundsut.locked, roundsut.created_at " + 
				"from roundsut " + 
				"where roundsut.msut_id = :msutId " + 
				"order by roundsut.id desc " + 
				"limit 1";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("msutId", msutId);
		
		roundSutModel = namedParameterJdbcTemplate.queryForObject(sql, mapSqlParameterSource, (rs, rowNum) ->
																										new RoundSutModel (
																								        		rs.getLong("id"),
																								        		rs.getLong("msut_id"),
																								        		rs.getShort("round"),
																								        		rs.getShort("locked"),
																								        		rs.getTimestamp("created_at")
																								));
		return roundSutModel;
	}

}
