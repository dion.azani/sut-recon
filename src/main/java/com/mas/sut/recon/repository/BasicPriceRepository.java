package com.mas.sut.recon.repository;

import java.util.List;

import com.mas.sut.recon.model.BasicPriceModel;
import com.mas.sut.recon.model.DbasicPriceModel;

public interface BasicPriceRepository {

	List<BasicPriceModel> getBasicPrice();
	BasicPriceModel getBasicPriceById(long basicPriceId);
	long insert(BasicPriceModel basicPriceModel);
	long update(BasicPriceModel basicPriceModel);
	long delete(long basicPriceId);
}
