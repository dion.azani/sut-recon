package com.mas.sut.recon.repository;

import com.mas.sut.recon.model.DsutColRowModel;

public interface DsutColRowRepository {

	int insert(DsutColRowModel dsutColRowModel);
	int update(DsutColRowModel dsutColRowModel);
}
