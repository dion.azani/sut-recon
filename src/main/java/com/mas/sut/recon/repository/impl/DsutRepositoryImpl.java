package com.mas.sut.recon.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mas.sut.recon.model.DsutModel;
import com.mas.sut.recon.model.RoundSutModel;
import com.mas.sut.recon.repository.DsutRepository;

@Repository
public class DsutRepositoryImpl implements DsutRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public long insert(DsutModel dsutModel) {

		long id = 0;
		
		String sql = "insert into dsut(name, code, msut_id, created_at) values (?, ?, ?, ?);";
		
		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {           

		                @Override
		                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		                	
		                    PreparedStatement ps = connection.prepareStatement(sql.toString(),
		                    		new String[] { "id" /* name of your id column */ });
		                    
		                    ps.setString(1, dsutModel.getName());
		                    ps.setString(2, dsutModel.getCode());
		                    ps.setLong(3, dsutModel.getMsutId());
		                    ps.setTimestamp(4, dsutModel.getCreatedAt());

		                    return ps;
		                }
		            }, holder);

		id = holder.getKey().longValue();
		
		return id;
	}

	@Override
	public List<DsutModel> getDsut(String codeCol, String codeRow, long msutId) {
 
		List<DsutModel> dsutModelList = new ArrayList<DsutModel>();
		
		String sql = "SELECT id, name, display_name, code, parent_id, msut_id, otorization_user_id, level, " +
				"is_total, name_short, created_at, created_by, updated_at, updated_by, dbmetadata_id, parent_code, " +
				"row_metadata_id, col_metadata_id, sut_code, total_formula, dmetadata_id, classification_id " + 
				"FROM dsut " +
				"where substring(dsut.code, 1, 6) in (:colCode, :rowCode) and msut_id = :msutId " + 
				"order by dsut.id";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("colCode", codeCol);
		mapSqlParameterSource.addValue("rowCode", codeRow);
		mapSqlParameterSource.addValue("msutId", msutId);
		
		dsutModelList = namedParameterJdbcTemplate.query(sql, mapSqlParameterSource, (rs, rowNum) ->
																										new DsutModel (
																								        		rs.getLong("id"),
																								        		rs.getString("name"),
																								        		rs.getString("display_name"),
																								        		rs.getString("code"),
																								        		rs.getLong("parent_id"),
																								        		rs.getLong("msut_id"),
																								        		rs.getLong("otorization_user_id"),
																								        		rs.getShort("level"),
																								        		rs.getBoolean("is_total"),
																								        		rs.getString("name_short"),
																								        		rs.getTimestamp("created_at"),
																								        		rs.getLong("created_by"),
																								        		rs.getTimestamp("updated_at"),
																								        		rs.getLong("updated_by"),
																								        		rs.getLong("dbmetadata_id"),
																								        		rs.getString("parent_code"),
																								        		rs.getLong("row_metadata_id"),
																								        		rs.getLong("col_metadata_id"),
																								        		rs.getString("sut_code"),
																								        		rs.getString("total_formula"),
																								        		rs.getLong("dmetadata_id"),
																								        		rs.getLong("classification_id")
																								));
		return dsutModelList;
	}

	@Override
	public List<DsutModel> getDsut(long colId, long rowId, long msutId) {
List<DsutModel> dsutModelList = new ArrayList<DsutModel>();
		
		String sql = "SELECT id, name, display_name, code, parent_id, msut_id, otorization_user_id, level, " +
				"is_total, name_short, created_at, created_by, updated_at, updated_by, dbmetadata_id, parent_code, " +
				"row_metadata_id, col_metadata_id, sut_code, total_formula, dmetadata_id, classification_id " + 
				"FROM dsut " +
				"where dsut.id in (:colId, :rowId) " + 
				"		and dsut.msut_id = 58";
		
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("colId", colId);
		mapSqlParameterSource.addValue("rowId", rowId);
		mapSqlParameterSource.addValue("msutId", msutId);
		
		dsutModelList = namedParameterJdbcTemplate.query(sql, mapSqlParameterSource, (rs, rowNum) ->
																										new DsutModel (
																								        		rs.getLong("id"),
																								        		rs.getString("name"),
																								        		rs.getString("display_name"),
																								        		rs.getString("code"),
																								        		rs.getLong("parent_id"),
																								        		rs.getLong("msut_id"),
																								        		rs.getLong("otorization_user_id"),
																								        		rs.getShort("level"),
																								        		rs.getBoolean("is_total"),
																								        		rs.getString("name_short"),
																								        		rs.getTimestamp("created_at"),
																								        		rs.getLong("created_by"),
																								        		rs.getTimestamp("updated_at"),
																								        		rs.getLong("updated_by"),
																								        		rs.getLong("dbmetadata_id"),
																								        		rs.getString("parent_code"),
																								        		rs.getLong("row_metadata_id"),
																								        		rs.getLong("col_metadata_id"),
																								        		rs.getString("sut_code"),
																								        		rs.getString("total_formula"),
																								        		rs.getLong("dmetadata_id"),
																								        		rs.getLong("classification_id")
																								));
		return dsutModelList;
	}

}
