package com.mas.sut.recon.repository;

import java.util.List;

import com.mas.sut.recon.model.DbasicPriceModel;

public interface DbasicPriceRepository {
	List<DbasicPriceModel> getDbasicPriceByBasicPriceId(long basicPriceId);
	long insert(DbasicPriceModel dbasicPriceModel);
	long update(DbasicPriceModel dbasicPriceModel);
	long delete(long basicPriceId);
}

