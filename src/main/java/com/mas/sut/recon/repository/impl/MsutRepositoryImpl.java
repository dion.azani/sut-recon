package com.mas.sut.recon.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mas.sut.recon.model.MsutModel;
import com.mas.sut.recon.repository.MsutRepository;

@Repository
public class MsutRepositoryImpl implements MsutRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public long insert(MsutModel msutModel) {
		
		long id = 0;
		
		String sql = "INSERT INTO msut (name, year, created_at) " +
				"VALUES (?, ?, ?)";
		
		KeyHolder holder = new GeneratedKeyHolder();

		jdbcTemplate.update(new PreparedStatementCreator() {           

		                @Override
		                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		                	
		                    PreparedStatement ps = connection.prepareStatement(sql.toString(),
		                    		new String[] { "id" /* name of your id column */ });
		                    
		                    ps.setString(1, msutModel.getName());
		                    ps.setString(2, msutModel.getYear());
		                    ps.setTimestamp(3, msutModel.getCreatedAt());

		                    return ps;
		                }
		            }, holder);

		id = holder.getKey().longValue();
		
		return id;
	}

}
