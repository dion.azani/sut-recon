package com.mas.sut.recon.controller;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mas.sut.recon.autobalras.dto.AutoBalRasDto;
import com.mas.sut.recon.autobalras.service.AutoBalRasService;
import com.mas.sut.recon.dto.Response;
import com.mas.sut.recon.yearrebasing.service.YearRebasingService;

import lombok.Data;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/autobal-ras")
public class AutoBalRasController {
	
	@Autowired
	AutoBalRasService autoBalRasService;
	
	@GetMapping("/ping")
	public ResponseEntity<Response> ping() {
		Response response = this.autoBalRasService.ping();
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		year = year + 1;
		
		response.setResponseMessage(String.valueOf(year));
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
	}
	
	@PostMapping("/v1/do")
	public ResponseEntity<Response> doAutoBalRas(@RequestBody AutoBalRasDto autoBalRasDto) {
		Response response = new Response();
		response.setSuccess(false);
		response.setResponseCode("002");
		response.setResponseMessage("");
		
		try {
			response = this.autoBalRasService.doAutoBalRas(autoBalRasDto);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("003");
			response.setResponseMessage(ex.getMessage());
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
	}
	
	@PostMapping("/v1/do-nm")
	public ResponseEntity<Response> doAutoBalRasNm(@RequestBody AutoBalRasDto autoBalRasDto) {
		Response response = new Response();
		response.setSuccess(false);
		response.setResponseCode("002");
		response.setResponseMessage("");
		
		try {
			response = this.autoBalRasService.doAutoBalRasNm(autoBalRasDto);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("003");
			response.setResponseMessage(ex.getMessage());
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
	}
}
