package com.mas.sut.recon.controller;

import java.util.Calendar;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mas.sut.recon.dto.Response;
import com.mas.sut.recon.dto.ResponseData;
import com.mas.sut.recon.yearrebasing.dto.BasicPriceDto;
import com.mas.sut.recon.yearrebasing.dto.YearRebasingRequestDto;
import com.mas.sut.recon.yearrebasing.service.YearRebasingService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/year-rebasing")
public class YearRebasingController {

	@Autowired
	YearRebasingService yearRebasingService;
	
	@GetMapping("/ping")
	public ResponseEntity<Response> ping() {
		Response response = this.yearRebasingService.ping();
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		year = year + 1;
		
		response.setResponseMessage(String.valueOf(year));
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
	}
	
	@PostMapping("/do")
	public ResponseEntity<Response> doYearRebasing(@RequestBody YearRebasingRequestDto yearRebasingRequestDto) {
		
		ResponseData response = new ResponseData();
		
		try {
			response = this.yearRebasingService.doYearRebasing(yearRebasingRequestDto);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("002");
			response.setResponseMessage(ex.getMessage());
			response.setData(null);
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
	}
	
	@PostMapping("/v2/do")
	public ResponseEntity<String> asyncDoYearRebasing(@RequestBody YearRebasingRequestDto yearRebasingRequestDto) {
		
		CompletableFuture<String> responseCf = null;
		String message = "true";
		
		try {
			long msutId = yearRebasingRequestDto.getMsutId();
			long basicPriceId = yearRebasingRequestDto.getBasicPriceId();
			String newName = yearRebasingRequestDto.getNewName();
			
			responseCf = this.yearRebasingService.asyncDoYearRebasing(msutId, basicPriceId, newName);
		}
		catch(Exception ex) {
			message = ex.getMessage();
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(message);
	}
	
	@PostMapping("/v3/do")
	public ResponseEntity<ResponseData> doYearRebasing_v3(@RequestBody YearRebasingRequestDto yearRebasingRequestDto) {
		ResponseData response = new ResponseData();
		
		try {
			
			long msutId = yearRebasingRequestDto.getMsutId();
			long basicPriceId = yearRebasingRequestDto.getBasicPriceId();
			String newName = yearRebasingRequestDto.getNewName();
			
			response = this.yearRebasingService.doYearRebasing_v3(msutId, basicPriceId, newName);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("002");
			response.setResponseMessage(ex.getMessage());
			response.setData(null);
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
	}
	
	@PostMapping("/basicPrice")
	public ResponseEntity<Response> createBasicPrice(@RequestBody BasicPriceDto basicPriceDto) {
		
		Response response = new Response();
		
		try {
			response = this.yearRebasingService.createBasicPrice(basicPriceDto);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("003");
			response.setResponseMessage(ex.getMessage());
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
		
	}
	
	@PutMapping("/basicPrice")
	public ResponseEntity<Response> updateBasicPrice(@RequestBody BasicPriceDto basicPriceDto) {
		
		Response response = new Response();
		
		try {
			response = this.yearRebasingService.updateBasicPrice(basicPriceDto);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("003");
			response.setResponseMessage(ex.getMessage());
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
		
	}
	
	@DeleteMapping("/basicPrice/{basicPriceId}")
	public ResponseEntity<Response> deleteBasicPrice(@PathVariable long basicPriceId) {
		
		Response response = new Response();
		
		try {
			response = this.yearRebasingService.deleteBasicPrice(basicPriceId);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("003");
			response.setResponseMessage(ex.getMessage());
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
		
	}
	
	@GetMapping("/basicPrice")
	public ResponseEntity<Response> getBasicPriceById(@RequestParam long basicPriceId) {
		
		Response response = new Response();
		
		try {
			response = this.yearRebasingService.getBasicPriceById(basicPriceId);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("003");
			response.setResponseMessage(ex.getMessage());
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
		
	}
	
	@GetMapping("/basicPrice/list")
	public ResponseEntity<Response> getBasicPrice() {
		
		Response response = new Response();
		
		try {
			response = this.yearRebasingService.getBasicPrice();
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("003");
			response.setResponseMessage(ex.getMessage());
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
		
	}
	
	@GetMapping("/basicPrice/view")
	public ResponseEntity<Response> getViewBasicPriceList(@RequestParam long basicPriceId) {
Response response = new Response();
		
		try {
			response = this.yearRebasingService.getViewBasicPriceList(basicPriceId);
		}
		catch(Exception ex) {
			response.setSuccess(false);
			response.setResponseCode("003");
			response.setResponseMessage(ex.getMessage());
		}
		
		return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
	}
	
}
