package com.mas.sut.recon.dto;

import lombok.Data;

@Data
public class Response {
	private boolean success;
	private String responseCode;
	private String responseMessage;
	private String clientMessage;
	private long timeStamp;
}
