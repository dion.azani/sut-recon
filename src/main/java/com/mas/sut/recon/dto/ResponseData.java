package com.mas.sut.recon.dto;

import java.util.List;

import lombok.Data;

@Data
public class ResponseData<T> extends Response {

	private List<T> data;
}
