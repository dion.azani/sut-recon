package com.mas.sut.recon.utils;

import java.sql.Timestamp;
import java.util.Calendar;

public class FormatUtils {

	public Timestamp getCurrentTimestamp() {
		Calendar calendar = Calendar.getInstance();
	    java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime().getTime());
	    
	    return currentTimestamp;
	}
}
