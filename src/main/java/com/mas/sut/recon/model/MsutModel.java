package com.mas.sut.recon.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MsutModel {

	private long id;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private String description;
	private boolean isSupplyCreated;
	private boolean isUseCreated;
	private String name;
	private String year;
	private long structure;
	private long createdBy;
	private long updatedBy;
	private long metadataId;
	private long assignmentId;
	private long yrBasePrice;
}
