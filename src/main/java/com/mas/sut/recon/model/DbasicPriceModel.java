package com.mas.sut.recon.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DbasicPriceModel {

	private String codeCol;
	private String codeRow;
	private int deflatorValue;
	private long basicPriceId;
}
