package com.mas.sut.recon.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BasicPriceModel {

	private long id;
	private String name;
	private int year;
	private long metadataId;
}
