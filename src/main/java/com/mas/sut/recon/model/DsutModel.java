package com.mas.sut.recon.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DsutModel {

	private long id;
	private String name;
	private String displayName;
	private String code;
	private long parentId;
	private long msutId;
	private long otorizationUserId;
	private short level;
	private boolean isTotal;
	private String nameShort;
	private Timestamp createdAt;
	private long createdBy;
	private Timestamp updatedAt;
	private long updatedBy;
	private long dbmetadataId;
	private String parentCode;
	private long rowMetadataId;
	private long colMetadataId;
	private String sutCode;
	private String totalFormula;
	private long dmetadataId;
	private long classificationId;
	
}
