package com.mas.sut.recon.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoundSutModel {

	private long id;
	private long msutId;
	private short round;
	private short locked;
	private Timestamp createdAt;
}
