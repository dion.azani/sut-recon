package com.mas.sut.recon.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DsutColRowModel {

	private long id;
	private long dsutColId;
	private long dsutRowId;
	private double value;
	private Timestamp valueCalculatedAt;
	private long mSutId;
	private long roundSutId;
	private Timestamp createdAt;
	private Timestamp createdBy;
	private Timestamp updatedAt;
	private Timestamp updatedBy;
	private short isLocked;
	private long isLockedBy;
	private float adjustmentFromPeviousRound;
	private Timestamp adjustedAt;
	private long adjustedBy;
	private String adjustmentReason;
	private short isApprovedNeeded;
	private String reqApprovalReason;
	private short isApproved;
	private Timestamp approvedAt;
	private long approvedBy;
	private Timestamp approvalRequestAt;
	private long approvalRequestBy;
	private short locked;
	private Timestamp lockedAt;
	private long lockedBy;
	private String lockedReason;
	private Timestamp adjustmentAt;
	private long adjustmentBy;
	private float adjustmentFromPreviousRound;
}
